package com.example.vitotamma.keamanankomputer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class HalamanUtama extends AppCompatActivity {


    protected static final String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 "; // 63 karakter

    String teksEnkripsi = ""; // untuk nyimpen hasil enkripsi

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman_utama);
    }

    public void enkrip(View view) {
        String enkrip[][]; // arraynya transposisi enkrip
        // ENKRIPSI CAESAR
        // mengambil nilai plain text dan key
        EditText plain = (EditText) findViewById(R.id.plain_text);
        String plainText = plain.getText().toString();

        EditText keytext = (EditText) findViewById(R.id.key);
        String key = keytext.getText().toString();

        // nambahin biar hurufnya gak ada yang hilang
        if (key.length() == 1){
            plainText = plainText + "..";
        }else plainText = plainText + ".";

        // caesar algotihm
        String cipherText = ""; // inisialisasi nilai chipher text
        for (int i = 0; i < plainText.length(); i++) { // looping sebanyak sepanjang tulisan plain text hasil di tambahi .
            int charPosition = ALPHABET.indexOf(plainText.charAt(i)); // nyari letak huruf ke berapa di ALPHABET
            int keyVal = (key.length() + charPosition) % 63; // tambah nilai yang diatasnya di tambah panjang key, hasilnya di modulo 63(panjang ALPHABET)
            char replaceVal = ALPHABET.charAt(keyVal); // ganti tiap huruf di plain text dengan nilai di keyval
            cipherText += replaceVal; // disatuin semua hasil ekripsi
        }
        // ganti kata di xml yang awal jadi nilai cipher text
//        TextView hasilenkripsicaesar = (TextView) findViewById(R.id.hasilenkripcaesar);
//        hasilenkripsicaesar.setText(cipherText);


        // ENKRIPSI TRANSPOSISI SQUARE
        double banyakbaris =((double)((double)cipherText.length()/(double)key.length())); // bagi panjang hasil enkripsi di caesar
        int baris = (int)Math.ceil(banyakbaris); // double jadikan int tapi kalau ada koma naik keatas (2.1 -> 3) jadikan nilai baris
        int kolom = key.length(); // ngambil panjang key untuk panjang kolom

        enkrip = new String[baris][kolom]; // bikin array untuk enkripsi
        String[] lineArray = cipherText.split("");  // hasil enkripsi dari caesar di masukkan ke array per hurufnya
        int counter = 0; // inisialisasi counter
        for (int i = 0; i<baris; i++){ // loop untuk banyak baris (ke bawah)
            for(int j=0; j<kolom; j++){ // loop untuk kolom (ke kanan)
                if(counter >= lineArray.length){ // kalau nilai counter lepih panjang dari panjang array lineArray
                    enkrip[i][j] = "*"; // tambah * di kolom yang berlebih tadi
                }else{
                    enkrip[i][j] = lineArray[counter]; // baris dan kolom tiap i dan j di isi oleh huruf di tiap lineArray + masukkan nilai dari kiri ke kanan dari atas  kebawah
                    counter++; // counter nambah

                }
            }
        }

        String result = ""; // inisialisasi nilai result
        for (int i = 0; i<kolom; i++){
            for(int j=0; j<baris; j++){
                    result=result + enkrip[j][i]; // ambil nilai tiap kolom dan baris baca dari atas kebawah kiri ke kanan tetapi diawali dengan kolom terakhir

            }

        }
        // ganti kata di xml yang awal jadi nilai result
        EditText hasilenkripsitranposisi = findViewById(R.id.hasilenkriptransposisi);
        hasilenkripsitranposisi.setText(result);

        this.teksEnkripsi = result; // menyimpan nilai hasil result ke nilai teksEnkripsi yang di atas

//        EditText hasil_enkrip = findViewById(R.id.decrypted_text);
//        hasil_enkrip.setText(teksEnkripsi);
    }








    public void dekrip(View view){
        String dekrip[][]; // arraynya transposisi dekrip
        //DEKRIPSI TRANSPOSISI SQUARE
        // mengambil nilai decrypted text text dan keydecrypt
        EditText decrypted_text = (EditText) findViewById(R.id.decrypted_text);
        String teksDekripsi = decrypted_text.getText().toString();

        EditText keytext = (EditText) findViewById(R.id.keydecrpt);
        String key = keytext.getText().toString();


        double banyakbaris =((double)((double)teksDekripsi.length()/(double)key.length()));// bagi panjang hasil enkripsi di caesar
        int baris = (int)Math.ceil(banyakbaris); // double jadikan int tapi kalau ada koma naik keatas (2.1 -> 3) jadikan nilai baris
        int kolom = key.length();// ngambil panjang key untuk panjang kolom

        dekrip = new String[baris][kolom];// bikin array untuk dekripsi
        String[] lineArray = teksDekripsi.split(""); // hasil enkripsi di masukkan ke array per hurufnya
        int counter = 0; // inisialisasi counter
        for (int i = 0; i<kolom; i++){ // loop untuk banyak baris (ke bawah)
            for(int j=0; j<baris; j++){ // loop untuk kolom (ke kanan)
                    dekrip[j][i] = lineArray[counter]; // baris dan kolom tiap i dan j di isi oleh huruf di tiap lineArray + masukkan nilai dari kiri ke kanan dari atas  kebawah
                    counter++;
            }
        }

        String result = "";
        for (int i = 0; i<baris; i++){
            for(int j = 0; j<kolom; j++){
                result+= dekrip[i][j];// ambil nilai tiap kolom dan baris baca dari atas kebawah kiri ke kanan tetapi diawali dengan kolom terakhir
            }

        }
        result = result.replaceAll("\\*","");
//        TextView hasildekripsitranposisi = (TextView) findViewById(R.id.hasildekriptransposisi);
//        hasildekripsitranposisi.setText(result);


        String plainTextTranpos = ""; //insialisasi plainTexttranpos
        for (int i = 0; i < result.length(); i++) {
            int charPosition = ALPHABET.indexOf(result.charAt(i));// nyari letak huruf ke berapa di ALPHABET
            int keyVal = (charPosition - key.length()) % 63; // tambah nilai yang diatasnya di kurangi panjang key, hasilnya di modulo 63(panjang ALPHABET)
            if (keyVal < 0)
            {
                keyVal = ALPHABET.length() + keyVal;
            }
            char replaceVal = ALPHABET.charAt(keyVal);//balikkan nilai hasil dekrip transposisi ke plain text awal
            plainTextTranpos += replaceVal;
        }
        EditText hasilDekripsi = findViewById(R.id.hasildekripcaesar);
        hasilDekripsi.setText(plainTextTranpos);


    }


}
